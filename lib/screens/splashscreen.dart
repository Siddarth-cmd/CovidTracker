// ignore_for_file: use_key_in_widget_constructors, avoid_unnecessary_containers, prefer_const_constructors, must_be_immutable

import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

class SplashScreen extends StatelessWidget {
  int duration = 0;
  Widget next;
  SplashScreen({required this.duration, required this.next});
  @override
  Widget build(BuildContext context) {
    Future.delayed(Duration(seconds: duration), () {
      Navigator.push(
        context,
        MaterialPageRoute(builder: (context) => next),
      );
    });
    return Scaffold(
      backgroundColor: Color(0xFF329E97),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Container(
            child: Center(
              child: Image.asset(
                'assets/images/image.png',
                height: 100,
                width: 100,
              ),
            ),
          ),
          SizedBox(
            height: 10,
          ),
          SpinKitCircle(
            color: Colors.yellowAccent,
          ),
          SizedBox(
            height: 60,
          ),
          Container(
            child: Center(
              child: Image.asset(
                'assets/images/srm.png',
                height: 200,
                width: 200,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
