// ignore_for_file: camel_case_types, unused_element, prefer_const_constructors, prefer_const_literals_to_create_immutables

import 'package:covidtracker/screens/learn.dart';
import 'package:covidtracker/screens/faq.dart';
import 'package:covidtracker/screens/statistics.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:covidtracker/screens/worldwide.dart';

class trackerScreen extends StatefulWidget {
  const trackerScreen({Key? key}) : super(key: key);

  @override
  State<trackerScreen> createState() => _trackerScreenState();
}

class _trackerScreenState extends State<trackerScreen> {
  int currentindex = 0;
  final screen = [
    WorldWide(),
    CountryPage(),
    Faq(),
    Learn(),
  ];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: IndexedStack(
        index: currentindex,
        children: screen,
      ),
      bottomNavigationBar: BottomNavigationBar(
        currentIndex: currentindex,
        onTap: (index) => setState(() => currentindex = index),
        items: [
          BottomNavigationBarItem(
            icon: Icon(FontAwesomeIcons.globe),
            label: "WorldWide",
            backgroundColor: Color(0xFF37B0AD),
          ),
          BottomNavigationBarItem(
              icon: Icon(FontAwesomeIcons.chartArea),
              label: "Statistics",
              backgroundColor: Colors.red),
          BottomNavigationBarItem(
              icon: Icon(FontAwesomeIcons.question),
              label: "FAQ",
              backgroundColor: Colors.green),
          BottomNavigationBarItem(
            icon: Icon(FontAwesomeIcons.memory),
            label: "Myth Busters",
            backgroundColor: Colors.amberAccent,
          ),
        ],
      ),
    );
  }
}
