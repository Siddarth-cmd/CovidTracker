// ignore_for_file: prefer_const_constructors, camel_case_types, use_key_in_widget_constructors, avoid_unnecessary_containers, sized_box_for_whitespace, deprecated_member_use
// ignore_for_file: prefer_const_literals_to_create_immutables
import 'package:flutter/material.dart';
import 'package:animated_text_kit/animated_text_kit.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:covidtracker/screens/loginscreen.dart';
import 'package:covidtracker/screens/registrationscreen.dart';

class homepage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Column(
          children: [
            Padding(
              padding: EdgeInsets.all(30.0),
              child: Column(
                children: [
                  Row(
                    children: [
                      Text(
                        "COVID19 Tracker",
                        style: TextStyle(
                          fontSize: 25,
                          color: Color(0xFF454F63),
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      IconButton(
                        onPressed: () {},
                        icon: FaIcon(
                          FontAwesomeIcons.peopleArrows,
                        ),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 20.0,
                  ),
                ],
              ),
            ),
            Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Padding(
                  padding: EdgeInsets.only(left: 20.0),
                  child: Column(
                    children: [
                      Hero(
                        tag: 'logo',
                        child: Container(
                          child: Image.asset('assets/images/logo.png'),
                          height: 100.0,
                          width: 100.0,
                        ),
                      ),
                      SizedBox(
                        height: 30.0,
                      ),
                      TypewriterAnimatedTextKit(
                        text: ["You will connect anonymously"],
                        textStyle: TextStyle(
                          color: Color(0xFF454F63),
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      SizedBox(
                        height: 20.0,
                      ),
                      Padding(
                        padding: EdgeInsets.symmetric(vertical: 6.0),
                        child: Material(
                          color: Color(0xFF37B0AD),
                          borderRadius: BorderRadius.circular(30.0),
                          child: MaterialButton(
                            onPressed: () {
                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => LoginScreen()),
                              );
                            },
                            minWidth: 200.0,
                            height: 42.0,
                            child: Text("Login"),
                          ),
                        ),
                      ),
                      Text(
                        "Or",
                        style: TextStyle(
                          color: Color(0xFF454F63),
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.symmetric(vertical: 6.0),
                        child: Material(
                          color: Color(0xFF37B0AD),
                          borderRadius: BorderRadius.circular(30.0),
                          child: MaterialButton(
                            onPressed: () {
                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => Registration()),
                              );
                            },
                            minWidth: 200.0,
                            height: 42.0,
                            child: Text("Register"),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
